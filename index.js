const express = require( 'express' );
const app = express();
const mongoose = require( `mongoose` );
const PORT = 5000;

// routers
const taskRoutes = require('./routes/taskRoutes');

// middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// database connection
mongoose.connect("mongodb+srv://lawrence:admin@cluster0.psfw2.mongodb.net/s31?retryWrites=true&w=majority",
    {
        useNewUrlParser:true,
	    useUnifiedTopology:true
    }
).then( ()=> { 
	console.log( `Successfully Connected to Database!` );
}).catch( (error)=> { 
	console.log( error );
});
 
// routes
app.use('/', taskRoutes);


app.listen( PORT, () => {
    console.log( `Server started on port ${PORT}` );
});