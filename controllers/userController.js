const { findByIdAndUpdate } = require('../models/User');
let User = require('../models/User');

module.exports.createUser = function(reqBody){
    
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        userName : reqBody.userName,
        password : reqBody.password
    });

    return newUser.save().then( ( savedUser, error) => {
        if(error){
            return error;
            // uses return since wala res and send then function
        }else{
            return savedUser;
        }
    })
}

module.exports.displayUsers = () => {

    return User.find().then ( (result, error) => {
        if(error){
            return error;
        }else{
            return result;
            // return an array of objects
        }
    } );

}

module.exports.getOneUser = (paramsId) => {
    return User.findById(paramsId).then( (result, error) => {
        if(error){
            return error;
        }else{
            return result;
        }
    } );
}

module.exports.updateUser = (paramsId, reqBody) => {

    return User.findByIdAndUpdate(paramsId, reqBody, {new: true}).then ((result, error) => {
        if(error){
            return error;
        }else{
            return result;
        }

    })    
}

module.exports.deleteUser = (paramsId) => {

    return User.findByIdAndDelete(paramsId).then( (result, error) => {
        if(error){
            return error;
        }else{
            return true;
        }
    })
}