// Routes
const express = require('express');
const router = express.Router();

// models 
let User = require('./../models/User');

// controllers
let userController = require('../controllers/userController')

// insert a new user
router.post('/add-task', (req, res) => {
    // console.log(req.body);
    // let newUser = new User({
    //     firstName : req.body.firstName,
    //     lastName : req.body.lastName,
    //     userName : req.body.userName,
    //     password : req.body.password
    // });

    // newUser.save( ( error, savedUser) => {
    //     if(error){
    //         res.send(error);
    //     }else{
    //         res.send(`New user saved:`, savedUser);
    //     }
    // })

    userController.createUser(req.body).then( result => res.send(result) )
});

// retrieve the user
router.get('/users', (req, res) => {

    // model.method
        // search users from the database
    // User.find({}, (error, result) => {
    //     if(error){
    //         res.send(error);
    //     }else{
    //         res.send(result);
    //         // return an array of objects
    //     }
    // });
    // send it as a response

    userController.displayUsers().then ( result => res.send(result) );
});

// retrieve a specific user
router.get('/users/:id', (req, res) => {
    // let params = req.params.id;
    // User.findById(params, (error, result) => {
    //     if(error){
    //         res.send(error);
    //     }else{
    //         res.send(result);
    //     }
    // })

    userController.getOneUser(req.params.id).then( result => res.send(result));
});

// update user's info
router.put('/users/:id', (req, res) => {
    // model.method
    // User.findByIdAndUpdate(req.params.id, req.body, {new: true}, (error, result) => {
    //     if(error){
    //         res.send(error);
    //     }else{
    //         res.send(result);
    //     }
    // })

    userController.updateUser(req.params.id, req.body).then( result => res.send(result));
});
// gamit ng new:true is para ang return is updated
// req.body is para kung unsay i update

// delete user
router.delete('/users/:id', (req, res) => {
    // model.method
    // let params =req.params.id;
    // User.findByIdAndDelete(params, (error, result) => {
    //     if(error){
    //         res.send(error);
    //     }else{
    //         res.send(true);
    //     }
    // })

    userController.deleteUser(req.params.id).then( result => res.send(result));
});
module.exports = router;